#!/usr/bin/env bash 
# 
# Using bash in the shebang rather than /bin/sh, which should
# be avoided as non-POSIX shell users (fish) may experience errors.

lxsession & # Login screen
picom --experimental-backends &
flashfocus &
#nitrogen --restore & # Restores background to the currently selected one
xwallpaper --zoom ~/.local/share/bg &
urxvtd -q -o -f &
/usr/bin/emacs --daemon &
volumeicon &
nm-applet &
kdeconnect-indicator & # Starts the kde connect notifcation service
xinput disable Atmel\ Atmel\ maXTouch\ Digitizer & # Disables touchscreen on 80k9 laptop
~/.local/bin/remaps & # Luke Smith's keybinds
xset r rate 300 50 &	# Speed xrate up
unclutter &		# Remove mouse when idle
dunst & # Lightweight notification service
redshift-gtk & # Adjusts the color temperature when the sun goes down
xfce4-power-manager & # Isn't autostarting on its own anymore for some reason
