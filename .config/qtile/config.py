# -*- coding: utf-8 -*-
import os
import re
import socket
import subprocess
import owm
import json
from libqtile import qtile
from libqtile.config import Click, Drag, Group, KeyChord, Key, Match, Screen
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy
from typing import List  # noqa: F401

mod = "mod4"                                     # Sets mod key to SUPER/WINDOWS
myTerm = "alacritty"                             # My terminal of choice
myFM = "pcmanfm"
myBrowser = "brave"
myScreencap = "flameshot gui"
home = os.path.expanduser('~')
prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

keys = [
         ### The essentials
         Key([mod], "Return",
             lazy.spawn(myTerm),
             desc='Launches My Terminal'
             ),
         Key([mod], "d",
             lazy.spawn("dmenu_run -p 'Run: '"),
             # lazy.spawn("rofi -show drun -config ~/.config/rofi/themes/dt-dmenu.rasi -display-drun \"Run: \" -drun-display-format \"{name}\""),
             desc='Run Launcher'
             ),
         Key([mod], "Tab",
             lazy.next_layout(),
             desc='Toggle through layouts'
             ),
         Key([mod], "q",
             lazy.window.kill(),
             desc='Kill active window'
             ),
         Key([mod, "shift"], "r",
             lazy.restart(),
             desc='Restart Qtile'
             ),
         Key([mod, "shift"], "q",
             lazy.shutdown(),
             desc='Shutdown Qtile'
             ),
         Key(["control", "shift"], "e",
             lazy.spawn("emacsclient -c -a emacs"),
             desc='Doom Emacs'
             ),
         Key([mod, "control"], "r",
            lazy.spawn(myFM),
            desc='Launches my file manager'
            ),
         Key([mod, "control"], "w",
            lazy.spawn(myBrowser),
            desc='Launches my web browser'
            ),
         Key([mod, "control"], "s",
            lazy.spawn("fsearch"),
            desc='Launches fsearch'
            ),
         Key([mod], "Print",
            lazy.spawn(myScreencap),
            desc='Takes screencap'
            ),
         ### Switch focus to specific monitor (out of three)
         Key([mod], "w",
             lazy.to_screen(0),
             desc='Keyboard focus to monitor 1'
             ),
         Key([mod], "e",
             lazy.to_screen(1),
             desc='Keyboard focus to monitor 2'
             ),
         Key([mod], "r",
             lazy.to_screen(2),
             desc='Keyboard focus to monitor 3'
             ),
         ### Switch focus of monitors
         Key([mod], "period",
             lazy.next_screen(),
             desc='Move focus to next monitor'
             ),
         Key([mod], "comma",
             lazy.prev_screen(),
             desc='Move focus to prev monitor'
             ),
         ### Treetab controls
          Key([mod, "shift"], "h",
             lazy.layout.move_left(),
             desc='Move up a section in treetab'
             ),
         Key([mod, "shift"], "l",
             lazy.layout.move_right(),
             desc='Move down a section in treetab'
             ),
         ### Window controls
         Key([mod], "k",
             lazy.layout.up(),
             desc='Move focus down in current stack pane'
             ),
         Key([mod], "j",
             lazy.layout.down(),
             desc='Move focus up in current stack pane'
             ),
         Key([mod, "shift"], "j",
             lazy.layout.shuffle_down(),
             lazy.layout.section_down(),
             desc='Move windows down in current stack'
             ),
         Key([mod, "shift"], "k",
             lazy.layout.shuffle_up(),
             lazy.layout.section_up(),
             desc='Move windows up in current stack'
             ),
         Key([mod], "h",
             lazy.layout.shrink(),
             lazy.layout.decrease_nmaster(),
             desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
             ),
         Key([mod], "l",
             lazy.layout.grow(),
             lazy.layout.increase_nmaster(),
             desc='Expand window (MonadTall), increase number in master pane (Tile)'
             ),
         Key([mod], "n",
             lazy.layout.normalize(),
             desc='normalize window size ratios'
             ),
         Key([mod], "m",
             lazy.layout.maximize(),
             desc='toggle window between minimum and maximum sizes'
             ),
         Key([mod, "shift"], "f",
             lazy.window.toggle_floating(),
             desc='toggle floating'
             ),
         Key([mod], "f",
             lazy.window.toggle_fullscreen(),
             desc='toggle fullscreen'
             ),
         ### Stack controls
         Key([mod, "shift"], "Tab",
             lazy.layout.rotate(),
             lazy.layout.flip(),
             desc='Switch which side main pane occupies (XmonadTall)'
             ),
          Key([mod], "space",
             lazy.layout.next(),
             desc='Switch window focus to other pane(s) of stack'
             ),
         Key([mod, "shift"], "space",
             lazy.layout.toggle_split(),
             desc='Toggle between split and unsplit sides of stack'
             ),
         # Emacs programs launched using the key chord CTRL+e followed by 'key'
         KeyChord(["control"],"e", [
             Key([], "e",
                 lazy.spawn("emacsclient -c -a 'emacs'"),
                 desc='Launch Emacs'
                 ),
             Key([], "b",
                 lazy.spawn("emacsclient -c -a 'emacs' --eval '(ibuffer)'"),
                 desc='Launch ibuffer inside Emacs'
                 ),
             Key([], "d",
                 lazy.spawn("emacsclient -c -a 'emacs' --eval '(dired nil)'"),
                 desc='Launch dired inside Emacs'
                 ),
             Key([], "i",
                 lazy.spawn("emacsclient -c -a 'emacs' --eval '(erc)'"),
                 desc='Launch erc inside Emacs'
                 ),
             Key([], "m",
                 lazy.spawn("emacsclient -c -a 'emacs' --eval '(mu4e)'"),
                 desc='Launch mu4e inside Emacs'
                 ),
             Key([], "n",
                 lazy.spawn("emacsclient -c -a 'emacs' --eval '(elfeed)'"),
                 desc='Launch elfeed inside Emacs'
                 ),
             Key([], "s",
                 lazy.spawn("emacsclient -c -a 'emacs' --eval '(eshell)'"),
                 desc='Launch the eshell inside Emacs'
                 ),
             Key([], "v",
                 lazy.spawn("emacsclient -c -a 'emacs' --eval '(+vterm/here nil)'"),
                 desc='Launch vterm inside Emacs'
                 )
         ]),
         # Dmenu scripts launched using the key chord SUPER+p followed by 'key'
         KeyChord([mod], "p", [
             Key([], "e",
                 lazy.spawn("./dmscripts/dmconf"),
                 desc='Choose a config file to edit'
                 ),
             Key([], "Print",
                 lazy.spawn("./dmscripts/maimpick"),
                 desc='Take screenshots via dmenu'
                 ),
             Key(["control"], "Print",
                 lazy.spawn("./dmscripts/dmenurecord"),
                 desc='Take video recordings via dmenu'
                 ),
             Key([], "k",
                 lazy.spawn("./dmscripts/dmkill"),
                 desc='Kill processes via dmenu'
                 ),
             Key([], "l",
                 lazy.spawn("./dmscripts/dmlogout"),
                 desc='A logout menu'
                 ),
             Key([], "m",
                 lazy.spawn("./dmscripts/dman"),
                 desc='Search manpages in dmenu'
                 ),
             Key([], "o",
                 lazy.spawn("./dmscripts/dmqute"),
                 desc='Search your qutebrowser bookmarks and quickmarks'
                 ),
             Key([], "r",
                 lazy.spawn("./dmscripts/dmred"),
                 desc='Search reddit via dmenu'
                 ),
             Key([], "s",
                 lazy.spawn("./dmscripts/dmsearch"),
                 desc='Search various search engines via dmenu'
                 ),
             Key([], "p",
                 lazy.spawn("passmenu"),
                 desc='Retrieve passwords with dmenu'
                 ),
             Key([], "j",
                 lazy.spawn("./dmscripts/dmenuunicode"),
                 desc='Opens list of emojis to copy to clipboard'
                 ),

			 # INCREASE/DECREASE BRIGHTNESS
    Key([], "XF86MonBrightnessUp", lazy.spawn("xbacklight -inc 5")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("xbacklight -dec 5")),
         ])
]

group_names = [
               ("1", {'layout': 'monadtall'}),
               ("2", {'layout': 'monadtall'}),
               ("3", {'layout': 'monadtall'}),
               ("4", {'layout': 'monadtall'}),
               ("5", {'layout': 'monadtall'}),
               ("6", {'layout': 'monadtall'}),
               ("7", {'layout': 'monadtall'}),
               ("8", {'layout': 'monadtall'}),
               ("9", {'layout': 'monadtall'}),
    ]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group

##### Import Pywal Palette #####
with open(home + '/.cache/wal/colors.json') as json_file:
    data = json.load(json_file)
    colorsarray = data['colors']
    val_list = list(colorsarray.values())
    def getList(val_list):
        return [*val_list]

def init_colors():
    return [*val_list]

colors = init_colors()

#trying to make text always black
colors.append("#ffffff"),

##### End Import Pywal Palette #####

layout_theme = {"border_width": 2,
                "margin": 8,
#                "border_focus": "e1acff",
                "border_focus": colors[2],
                "border_normal": "1D2330"
                }

layouts = [
    #layout.MonadWide(**layout_theme),
    layout.Bsp(**layout_theme),
    #layout.Stack(stacks=2, **layout_theme),
    #layout.Columns(**layout_theme),
    #layout.RatioTile(**layout_theme),
    layout.Tile(shift_windows=True, **layout_theme),
    #layout.VerticalTile(**layout_theme),
    layout.Matrix(**layout_theme),
    #layout.Zoomy(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.Max(**layout_theme),
    layout.Stack(num_stacks=2),
    layout.RatioTile(**layout_theme),
    # layout.TreeTab(
    #      font = "Ubuntu",
    #      fontsize = 10,
    #      sections = ["FIRST", "SECOND", "THIRD", "FOURTH"],
    #      section_fontsize = 10,
    #      border_width = 2,
    #      bg_color = "1c1f24",
    #      active_bg = "c678dd",
    #      active_fg = "000000",
    #      inactive_bg = "a9a1e1",
    #      inactive_fg = "1c1f24",
    #      padding_left = 0,
    #      padding_x = 0,
    #      padding_y = 5,
    #      section_top = 10,
    #      section_bottom = 20,
    #      level_shift = 8,
    #      vspace = 3,
    #      panel_width = 200
    #      ),
    layout.Floating(**layout_theme)
]

#######################################################################################################
# colors = [["#282c34", "#282c34"], # panel background                                                #
#           ["#3d3f4b", "#434758"], # background for current screen tab                               #
#           ["#ffffff", "#ffffff"], # font color for group names                                      #
#           ["#ff5555", "#ff5555"], # border line color for current tab                               #
#                                                                                                     #
# # red beach sunset theme                                                                            #
#           ["#D32C26", "#D32C26"], # border line color for 'other tabs' and color for 'odd widgets'  #
#           ["#741C12", "#741C12"], # color for the 'even widgets'                                    #
#                                                                                                     #
# # monster beach man theme                                                                           #
# #          ["#9EACBD", "#9EACBD"], # border line color for 'other tabs' and color for 'odd widgets' #
# #          ["#67778D", "#67778D"], # color for the 'even widgets'                                   #
#                                                                                                     #
# # mountain cloudy sunset theme                                                                      #
# #          ["#FD7D50", "#FD7D50"], # border line color for 'other tabs' and color for 'odd widgets' #
# #          ["#235161", "#235161"], # color for the 'even widgets'                                   #
#                                                                                                     #
#           ["#e1acff", "#e1acff"], # window name                                                     #
#           ["#ecbbfb", "#ecbbfb"]] # backbround for inactive screens                                 #
#######################################################################################################



prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

##### DEFAULT WIDGET SETTINGS #####
widget_defaults = dict(
#    font="Ubuntu Mono",
    font="Terminus Regular",
#    font="",
    fontsize = 12,
#    fontsize = 11,
    padding = 2,
    background=colors[2]
)
extension_defaults = widget_defaults.copy()

def init_widgets_list():
    widgets_list = [
              widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[16],
                       background = colors[0]
                       ),
              widget.Image(
                       filename = "~/.config/qtile/icons/arch-white.png",
                       scale = "False",
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm)}
                       ),
             widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[16],
                       background = colors[0]
                       ),
              widget.GroupBox(
                       font = "Ubuntu Bold",
                       fontsize = 12,
                       margin_y = 3,
                       margin_x = 0,
                       padding_y = 5,
                       padding_x = 3,
                       borderwidth = 3,
                       active = colors[16],
                       inactive = colors[7],
                       rounded = False,
                       highlight_color = colors[1],
                       highlight_method = "line",
                       this_current_screen_border = colors[6],
                       this_screen_border = colors [4],
                       other_current_screen_border = colors[6],
                       other_screen_border = colors[4],
                       foreground = colors[6],
                       background = colors[0]
                       ),
              widget.Prompt(
                       prompt = prompt,
                       font = "Ubuntu Mono",
                       padding = 10,
                       foreground = colors[3],
                       background = colors[1]
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 40,
                       foreground = colors[2],
                       background = colors[0]
                       ),
              widget.WindowName(
                       foreground = colors[6],
                       background = colors[0],
                       padding = 0
                       ),
              widget.Systray(
                       background = colors[0],
                       padding = 5
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[0],
                       background = colors[0]
                       ),
              widget.TextBox(
                       text = " 🗲",
                       padding = 0,
                       foreground = colors[16],
                       background = colors[10],
                       fontsize = 12
                       ),
			  widget.Battery(
                       update_interval = 10,
                       foreground = colors[16],
                       background = colors[10],
			           show_short_text = False,
			           format = '{char} {percent:2.0%}',
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e sudo powertop')},
	                   ),
             # widget.TextBox(
             #          text = '',
             #          background = colors[0],
             #          foreground = colors[4],
             #          padding = 0,
             #          fontsize = 37
             #          ),
              owm.OpenWeatherMap(
                      api_key="8e2310f2a1fcc1b9c93b0f97ec48abdc",
                      latitude=40.2395556,
                      longitude=-75.5567222,
                      icon_font="Weather Icons",
                      units="imperial",
                      # format="{icon} {condition} ▎ 🌡{temp:.1f} ❆{temp_min} ♨{temp_max}",
                      format="{icon} {condition} ▎ 🌡{temp:.1f} 🗭{temp_feels_like} {humidity}%",
                      foreground = colors[16],
                      background = colors[9],
                      mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(' ./weather-notify.sh')},
                      ),
             # widget.NetGraph(
             #           interface = "wlp2s0",
             #           format = '{down} ↓↑ {up}',
             #           foreground = colors[2],
             #           background = colors[4],
             #           padding = 5
             #           ),
#              widget.TextBox(
#                       text = '',
#                       background = colors[4],
#                       foreground = colors[5],
#                       padding = 0,
#                       fontsize = 37
#                       ),
              widget.TextBox(
                       text = " 🖥",
                       padding = 2,
                       foreground = colors[16],
                       background = colors[10],
                       fontsize = 11
                       ),
              widget.ThermalSensor(
                      # tag_sensor="coretemp",
                       foreground = colors[16],
                       background = colors[10],
                       threshold = 90,
                       padding = 5
                       ),
#              widget.TextBox(
#                       text='',
#                       background = colors[5],
#                       foreground = colors[4],
#                       padding = 0,
#                       fontsize = 37
#                       ),
              widget.TextBox(
                       text = " ⟳",
                       padding = 2,
                       foreground = colors[16],
                       background = colors[9],
                       fontsize = 14
                       ),
              widget.CheckUpdates(
                       update_interval = 1800,
                       distro = "Arch_checkupdates",
                       display_format = "{updates} Updates",
                       foreground = colors[16],
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e sudo pacman -Syu')},
                       background = colors[9]
                       ),
#              widget.TextBox(
#                       text = '',
#                       background = colors[4],
#                       foreground = colors[5],
#                       padding = 0,
#                       fontsize = 37
#                       ),
              widget.TextBox(
                       text = " 🖬",
                       foreground = colors[16],
                       background = colors[10],
                       padding = 0,
                       fontsize = 14
                       ),
              widget.Memory(
                       foreground = colors[16],
                       background = colors[10],
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e bashtop')},
                       padding = 5
                       ),
#              widget.TextBox(
#                       text='',
#                       background = colors[5],
#                       foreground = colors[4],
#                       padding = 0,
#                       fontsize = 37
#                       ),
              widget.TextBox(
                       text = " ☀",
                       padding = 0,
                       foreground = colors[16],
                       background = colors[9],
                       fontsize = 12
                       ),
              widget.Backlight(
                       foreground = colors[16],
                       background = colors[9],
				       backlight_name = "intel_backlight",
                       ),
#              widget.TextBox(
#                       text = '',
#                       background = colors[4],
#                       foreground = colors[5],
#                       padding = 0,
#                       fontsize = 37
#                       ),
              widget.TextBox(
                      text = " 🕪",
                       foreground = colors[16],
                       background = colors[10],
                       padding = 0
                       ),
              widget.Volume(
                       foreground = colors[16],
                       background = colors[10],
                       padding = 5
                       ),
#              widget.TextBox(
#                       text = '',
#                       background = colors[5],
#                       foreground = colors[4],
#                       padding = 0,
#                       fontsize = 37
#                       ),
              widget.CurrentLayoutIcon(
                       custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
                       foreground = colors[0],
                       background = colors[9],
                       padding = 0,
                       scale = 0.7
                       ),
              widget.CurrentLayout(
                       foreground = colors[16],
                       background = colors[9],
                       padding = 5
                       ),
#              widget.TextBox(
#                       text = '',
#                       background = colors[4],
#                       foreground = colors[5],
#                       padding = 0,
#                       fontsize = 37
#                       ),
              widget.Clock(
                       foreground = colors[16],
                       background = colors[10],
                       format = "%A, %B %d %Y - %I:%M %p"
                       ),
              ]
    return widgets_list

def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    #del widgets_screen1[7:8]               # Slicing removes unwanted widgets (systray) on Monitors 1,3
    return widgets_screen1

def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    return widgets_screen2                 # Monitor 2 will display all widgets in widgets_list

def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=1.0, size=20)),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), opacity=1.0, size=20)),
            Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=1.0, size=20))]

if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()

def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)

def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)

def switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = True
cursor_warp = False

# floating_layout = layout.Floating(float_rules=[
#     # Run the utility of `xprop` to see the wm class and name of an X client.
#     # default_float_rules include: utility, notification, toolbar, splash, dialog,
#     # file_progress, confirm, download and error.
#     *layout.Floating.default_float_rules,
#     Match(title='Confirmation'),  # tastyworks exit box
#     Match(title='Qalculate!'),  # qalculate-gtk
#     Match(wm_class='kdenlive'),  # kdenlive
#     Match(wm_class='pinentry-gtk-2'),  # GPG key password entry
#     Match(wm_class='myxer'),  # Volume mixer
# ])

### FLOATING WINDOWS ###
floating_layout = layout.Floating(**layout_theme, float_rules=[
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
    {'wmclass': 'Lxappearance'},
    {'wmclass': 'Pavucontrol'},
    {'wmclass': 'VirtualBox Manager'},
    {'wmclass': 'Lutris'},
    {'wmclass': 'Wine'},
    {'wmclass': 'Thunar'},
    {'wmclass': 'net-technicpack-launcher-LauncherMain'},
    {'wmclass': 'Blueman-manager'},
    {'wmclass': 'feh'},
    {'wmclass': 'Lxpolkit'},
    {'wmclass': 'Timeshift-gtk'},
    {'wmclass': 'cemu.exe'},
    {'wmclass': 'origin.exe'},
    {'wmclass': 'explorer.exe'},
    {'wmclass': 'ffxivlauncher.exe'},
])

# Steam specific floating settings
@hook.subscribe.client_new
def float_steam(window):
    wm_class = window.window.get_wm_class()
    w_name = window.window.get_name()
    if (
        wm_class == ("Steam", "Steam")
        and (
            w_name != "Steam"
            # w_name == "Friends List"
            # or w_name == "Screenshot Uploader"
            # or w_name.startswith("Steam - News")
            or "PMaxSize" in window.window.get_wm_normal_hints().get("flags", ())
        )
    ):
        window.floating = True

auto_fullscreen = True
focus_on_window_activation = "smart"

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

#Adds window swallowing to qtile
import psutil

@hook.subscribe.client_new
def _swallow(window):
    pid = window.window.get_net_wm_pid()
    ppid = psutil.Process(pid).ppid()
    cpids = {c.window.get_net_wm_pid(): wid for wid, c in window.qtile.windows_map.items()}
    for i in range(5):
        if not ppid:
            return
        if ppid in cpids:
            parent = window.qtile.windows_map.get(cpids[ppid])
            parent.minimized = True
            window.parent = parent
            return
        ppid = psutil.Process(ppid).ppid()

@hook.subscribe.client_killed
def _unswallow(window):
    if hasattr(window, 'parent'):
        window.parent.minimized = False

# Brings all floating windows to the front
@lazy.function
def float_to_front(qtile):
    logging.info("bring floating windows to front")
    for group in qtile.groups:
        for window in group.windows:
            if window.floating:
                window.cmd_bring_to_front()
